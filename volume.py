# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 21:51:01 2015

@author: bernoulli
"""

import numpy as np
import math 
n=40
R=input("Radius of the sphere:")
theta=np.linspace(0,math.pi/2,n)  
v=0
for i in range(0,n-1):
    delta_theta=theta[i+1]-theta[i]
    v= v+ ((math.cos(theta[i]))**3)*(math.sin(theta[i])) * delta_theta 
v=(v*(16*math.pi*(R**3)))/3
print v
