from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np
#import copy
Nx = 13
Ny = 7
Matrix_prev = np.zeros((Ny,Nx))
Matrix_curr=np.zeros((Ny,Nx))
#np.array([[0 for x in range(12)] for x in range(9)]) 
#Matrix_prev[1:8, 0] = 99
Matrix_curr[1:Ny-1,0]=100.000

#Matrix_curr = Matrix_prev

while(np.linalg.norm(Matrix_curr -Matrix_prev)>1e-5):
    #Matrix_prev=copy.copy(Matrix_curr)
    Matrix_prev[:,:]=Matrix_curr[:,:]
    for i in range(1,Ny-1):
        for j in range(1,Nx-1):
            Matrix_curr[i,j]= ((Matrix_curr[i-1,j]+Matrix_curr[i-1,j+1]+Matrix_curr[i,j-1]+Matrix_curr[i+1,j])/4)
    
    #print np.linalg.norm(Matrix_curr -Matrix_prev)
print Matrix_curr[2,6]

fig = plt.figure()
ax = fig.gca(projection='3d')
X = np.arange(0, 6, 6.0/Nx)
Y = np.arange(0, 12, 12.0/Ny)
X, Y = np.meshgrid(X, Y)
#R = np.sqrt(X**2 + Y**2)
Z = Matrix_curr
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=1, antialiased=False)
ax.set_zlim(0, 100)

ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()
